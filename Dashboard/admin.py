'''
Created on May 2, 2019

@author: Darcy
'''
from django.contrib import admin

from .models import IpLocation


admin.site.register(IpLocation)