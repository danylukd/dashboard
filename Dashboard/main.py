'''
Created on May 2, 2019

@author: Darcy
'''
import json
import ipaddress
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.cache import never_cache

from Dashboard import dashboard_content
from ipaddress import ip_address

# Main entry point for the HTTP requests
@never_cache
def do_request(request):
    template = loader.get_template('Dashboard\main.html')
    if request.method == 'GET':
        context = {
            'ip_address': dashboard_content.request_ip(request),
            'dashboard_content_css': 'visibility: hidden;',
        }
    elif request.method == 'POST':
        data = request.POST.copy()
        ip_address = data.get('ip_address')
        
        valid_ip = validate_ip_address(ip_address)
        if valid_ip == 'OK':
            context = {'dashboard_content_css': 'visibility: visible;', }
            ip_loc_obj = dashboard_content.get_ip_location(ip_address)
            if ip_loc_obj:
                context.update(add_location_info(ip_loc_obj))
                context.update(add_weather_info(ip_loc_obj))
                language_code = request.META['HTTP_ACCEPT_LANGUAGE']
                if not language_code or len(language_code) < 2:
                    language_code = 'en'
                else:
                    language_code = language_code[0:2]
                context.update(add_news_info(ip_loc_obj, language_code))
            else:
                context = {'dashboard_content_css': 'visibility: hidden;', 'invalid_ip_message': 'IP does not have a geo-location',}    
        else:
            context = {'dashboard_content_css': 'visibility: hidden;', 'invalid_ip_message': valid_ip,}
    
    return HttpResponse(template.render(context, request))

# Validates the given IP string: 'OK' if acceptable or user-friendly error message
def validate_ip_address(ip):
    try:
        ipaddress.ip_address(ip)
        return 'OK'
    except ValueError as errorCode:
        return str(errorCode)


# Returns addable location info
def add_location_info(ip_loc_obj):
    context = {
        'ip_address': ip_loc_obj.ip_address,
        'lat': str(ip_loc_obj.lat),
        'lon': str(ip_loc_obj.lon),
        'city': ip_loc_obj.city,
        'region_code': ip_loc_obj.region_code,
    }
    return context


# Returns addable weather info
def add_weather_info(ip_loc_obj):
    weather_json = dashboard_content.get_weather_json(ip_loc_obj.city, ip_loc_obj.country_code)
    if weather_json:
        temperature = weather_json['main']['temp']
        weather_condition = weather_json['weather'][0]['description']
        weather_condition_icon = weather_json['weather'][0]['icon']
        weather_icon_url = 'https://openweathermap.org/img/w/' + weather_condition_icon + '.png'
        context = {
            'temperature': temperature,
            'weather_condition': weather_condition,
            'weather_icon_url': weather_icon_url,
        }
    return context


# Returns addable news info
def add_news_info(ip_loc_obj, language_code):
    top_headlines_json = dashboard_content.get_news_json(ip_loc_obj.country_code, language_code, 3)
    articles = []
    for article in top_headlines_json['articles']:
        news = HeadlineNews(title=article['title'], description=article['description'], url=article['url'], image_url=article['urlToImage']
                             )
        articles.append(news)
    context = {
            'articles_list': articles,
        }
    return context


# Data class to use in View
class HeadlineNews():

    def __init__(self, title, description, url, image_url):
        self.title = title
        self.description = description
        self.url = url
        self.image_url = image_url
    
