'''
Created on May 2, 2019

@author: Darcy
'''
import uuid

from django.db import models

# Entity class for a IP and geo-location information
class IpLocation(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    ip_address = models.CharField(db_index=True, max_length=39)
    lat = models.DecimalField(max_digits=7, decimal_places=5)
    lon = models.DecimalField(max_digits=8, decimal_places=5)
    country_code = models.CharField(max_length=2)
    # ideally this should be a separate look-up but cheat here for now
    country_name = models.CharField(max_length=200)
    region_code = models.CharField(max_length=2)
    city = models.CharField(max_length=200)
    date_created = models.DateTimeField(auto_now_add=True, blank=True)
    
    def __str__(self):
        return 'ip=' + self.ip_address + ',lat=' + str(self.lat)