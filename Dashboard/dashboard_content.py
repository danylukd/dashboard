'''
Created on May 3, 2019

@author: danylukd
'''

# ## Requires the following installed
# # pip install requests
# # pip install python-memcached
# # pip install newsapi-python

import requests
from newsapi import NewsApiClient
import memcache
from Dashboard import api_consts
from Dashboard.models import IpLocation

# Returns the request's IP; or will grab it from another resource if running on same server
def request_ip(request):
    ip_address = request.META.get('HTTP_X_FORWARDED_FOR') or request.META.get('REMOTE_ADDR')
    if not ip_address or ip_address == '127.0.0.1' or ip_address == '::1':
        # TODO: use a cookie to store IP info before making a network call
        ip_resp = requests.get('https://api.myip.com')
        if ip_resp.status_code == 200:
            ip_address = ip_resp.json()["ip"]
    return ip_address


# Returns the implementing memchae
def get_memcache():
    return memcache.Client(['127.0.0.1:11211'], debug=0)

# Returns the IpLocation model
def get_ip_location(ip):
    mc = get_memcache()
    
    ip_loc_obj = mc.get('ip_loc_obj:' + ip)
    if not ip_loc_obj:
        try:
            ip_loc_obj = IpLocation.objects.get(ip_address=ip)
        except IpLocation.DoesNotExist:
            ip_loc_obj = None
        
    if not ip_loc_obj:
        ip_location_resp = requests.get('http://api.ipstack.com/' + ip + '?access_key=' + api_consts.IPSTACK_API_KEY)
        if ip_location_resp.status_code == 200:
            ip_loc_resp_json = ip_location_resp.json()
            
            if ip_loc_resp_json['latitude'] and ip_loc_resp_json['longitude']:
                ip_loc_obj = IpLocation(ip_address = ip, lat = ip_loc_resp_json['latitude'],lon = ip_loc_resp_json['longitude'],
                                      country_code = ip_loc_resp_json['country_code'],country_name = ip_loc_resp_json['country_name'],
                                      region_code = ip_loc_resp_json['region_code'],city = ip_loc_resp_json['city'])
                ip_loc_obj.save()
                mc.set('ip_loc_obj:' + ip, ip_loc_obj, time=3600)

    else:
        mc.set('ip_loc_obj:' + ip, ip_loc_obj, time=3600)
        
    return ip_loc_obj


# Returns the weather JSON object
def get_weather_json(city, country_iso2):
    mc = get_memcache()
    weather_json = mc.get('weather_json:' + city + "." + country_iso2)
    
    if not weather_json:
        weather_query = 'https://api.openweathermap.org/data/2.5/weather?q=' + city + ',' + country_iso2 + '&cnt=1&units=metric&APPID=' + api_consts.OPENWEATHER_API_KEY
        weather_resp = requests.get(weather_query)
        if weather_resp.status_code == 200:
            weather_json = weather_resp.json()
            mc.set('weather_json:' + city + "." + country_iso2, weather_json, time=3600)
    
    return weather_json


# Returns the News
def get_news_json(country_iso2, language_code, number_entries):
    mc = get_memcache()
    top_headlines_json = mc.get('top_headlines_json:' + country_iso2 + "." + language_code)
    if not top_headlines_json:
        newsapi = NewsApiClient(api_key=api_consts.NEWSAPI_KEY)
        top_headlines_json = newsapi.get_top_headlines(country=country_iso2.lower(), language=language_code, 
                                                       category='general', page_size=number_entries)
        mc.set('top_headlines_json:' + country_iso2 + "." + language_code, top_headlines_json, time=3600)
        
    return top_headlines_json
